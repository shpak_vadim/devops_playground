import pytest

from libs.hello import fetch_hello


def test_prints_full_hello():
    got = fetch_hello("Ivan")

    assert got == "Hello: Ivan"